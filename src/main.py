"""
Laurent Colpaert - 24-03-2022 - Space Applications Services test
"""
import heapq
import json
import networkx as nx
import pandas as pd 
import matplotlib.pyplot as plt
import matplotlib as mpl
import warnings
#To avoid the warning of the append function of the panda library
warnings.simplefilter(action='ignore', category=FutureWarning)

def load():
    """
    Load the different json file and format their input
    """
    with open("input_files/problem_robot/states.json") as f:
        data = json.load(f)
    states = data["states"]
    with open("input_files/problem_robot/problem.json") as f:
        data = json.load(f)
    problem = data
    with open("input_files/problem_robot/actions.json") as f:
        data = json.load(f)
    df = pd.DataFrame(columns = ['action', 'state_start', 'state_end','time'])
    for action in data['actions']:
        df = df.append(action,ignore_index = True)

    return states,problem,df

def dijkstra(states,problem,df):
    """
    Create a graph and then use the dijkstra algorithm to find the shortest path
    @INPUT:
    - states (list) : a list of the possible state
    - problem (dictionnary) : a dictionnary containing the start and end state of the problem
    - df (DataFrame) : containing the action, state_start, state_end, time
    """
    #Create the graph
    graph = nx.DiGraph()

    states = set(df['state_start']).union(set(df['state_end']))
    graph.add_nodes_from(states)

    for _, row in df.iterrows():
        graph.add_edge(row['state_start'], row['state_end'], action=row['action'], time=row['time'])

    # Run the dijkstra algorithm
    return nx.dijkstra_path(graph, source=problem["init"], target=problem["goal"], weight='time')

def compute_time_taken(actions,df):
    """
    Compute the time taken for all the actions
    @INPUT:
    - actions (list) : a list containing all the actions to perform
    - df (DataFrame) : containing the action, state_start, state_end, time
    """
    return sum(
        df.loc[df["action"] == action]["time"].values[0] for action in actions
    )

def compute_action(states,df):
    """
    Transform the list of states transition into a list of actions 
    @INPUT:
    - states (list) : a list of the state transition
    - df (DataFrame) : containing the action, state_start, state_end, time
    """
    actions = []
    for i in range(len(states)-1):
        action = df.loc[(df["state_start"] == states[i]) & (df["state_end"] == states[i+1]), "action"].values[0]
        actions.append(action)
    actions.reverse()
    return actions 

def optimize(states,problem,df):
    """
    Runs the optimization algorithm as well as the actions taken and the time taken
    @INPUT:
    - states (list) : a list of the possible state
    - problem (dictionnary) : a dictionnary containing the start and end state of the problem
    - df (DataFrame) : containing the action, state_start, state_end, time
    """
    state_transitions = dijkstra(states,problem,df)
    actions = compute_action(state_transitions,df)
    time = compute_time_taken(actions,df)
    return actions,time

def generate_output(actions,time):
    """
    Generate the json file by creating a dictionnary with the output and giving it to the 'plan.json'
    """
    output = {
        "actions" : actions,
        "time" : time
    }
    with open('out/plan.json', 'w', encoding='utf-8') as f:
        json.dump(output, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    states, problem, df = load()
    actions, time = optimize(states,problem,df)
    generate_output(actions,time)
